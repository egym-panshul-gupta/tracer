package trace

import (
	"errors"
	"fmt"
	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"
	"math/rand"
	"net"
	"time"
)

// Exec executes the trace from source to destination with increasing time-to-live values
// for every ttl value tries the set number of times and stores the hop value for the minimum latency hop
// returns a slice of minimum latency hops or an error.
func Exec(options Options) ([]Hop, error) {

	// Calculating timeout from seconds to duration.
	timeout := time.Duration(float64(options.Timeout()) * float64(time.Second.Nanoseconds()))

	ttl := 1
	hops := []Hop{} // stores the hop information with the minimum latency out of the retries
	for hopId := 0; hopId < options.MaxHops(); hopId++ {
		var hopIp net.IP
		var minLatencyHop *Hop // stores the min latency hop from the retries for a single ttl value
		for try := 0; try < options.Retries(); try++ {
			// executes a single trace to destination with fixed ttl value
			hop, err := trace(ttl, options.DestinationIP(), options.SourceIP(), timeout, options.IpVersion())
			// Checks for:
			// err -> error in the execution process
			// hop.Error -> network errors
			// hop.IP -> will be nil if there are timeouts
			if err == nil && hop.IP != nil && hop.Error == nil {
				hopIp = hop.IP
				// fetch the DNS address for the hop ip
				hopDNS, dnsErr := net.LookupAddr(hop.IP.String())
				if dnsErr != nil || len(hopDNS) == 0 {
					hopDNS = append(hopDNS, hop.IP.String())
				}
				if minLatencyHop == nil || minLatencyHop.Distance > hop.Distance {
					minLatencyHop = hop
				}
				hop.TrySequence = hopId // store hop id in hop object for later use.
				fmt.Printf("[hop][try][ttl]: [%d][%d][%d]: %s (%s) -> %s \n", hopId, try, ttl, hop.IP.String(), hopDNS, hop.Distance.String())
			} else {
				// handle the errors in the trace execution step
				netErr := ""
				if err != nil {
					netErr = err.Error()
				} else {
					netErr = hop.Error.Error()
				}
				fmt.Printf("[hop][try][ttl]: [%d][%d][%d]: error: %s\n", hopId, try, ttl, netErr)
			}
		}
		// append the minimum latency hop among the retries to the list of hops towards the destination
		if minLatencyHop != nil {
			hops = append(hops, *minLatencyHop)
		}
		// check to see if we reached the destination
		if hopIp != nil && hopIp.Equal(options.DestinationIP()) {
			fmt.Printf("Destination (%s) reached in %d hops.\n", options.DestinationIP().String(), hopId+1)
			break
		}
		// increment the time-to-live for hop packet
		ttl++
	}
	return hops, nil
}

// CalculateMaxLatencyHop calculates the two consecutive hops with maximum latency between them from a list of hops
func CalculateMaxLatencyHop(hops []Hop) {
	maxAdjacentLatency := time.Duration(0.0)
	adjacentHops := make([]Hop, 2)
	fmt.Printf("Total hop points: %d\n", len(hops))
	for idx, hop := range hops {
		if idx+1 < len(hops) {
			adjacentLatency := hops[idx+1].Distance - hop.Distance
			if adjacentLatency < time.Duration(0.0) {
				adjacentLatency = adjacentLatency * -1 // sometimes the value can be negative, but we need it positive for correct comparison.
			}
			fmt.Printf("[hop][src][dest][latency]: %d %s %s %s\n", hops[idx+1].TrySequence, hop.IP.String(), hops[idx+1].IP.String(), adjacentLatency.String())
			if adjacentLatency > maxAdjacentLatency {
				maxAdjacentLatency = adjacentLatency
				adjacentHops[0] = hop
				adjacentHops[1] = hops[idx+1]
			}
		}
	}
	fmt.Println("Consecutive hops with maximum response time:")
	fmt.Printf("[hop][src][dest][latency]: %d %s %s %s\n", adjacentHops[1].TrySequence, adjacentHops[0].IP.String(), adjacentHops[1].IP.String(), maxAdjacentLatency)
}

// trace executes a single hop with defined ttl value for packet over the network.
func trace(ttl int, destination net.IP, source net.IP, timeout time.Duration, ipVersion int) (hop *Hop, err error) {

	request := []byte{}
	var publisherConn net.Conn
	protocol := ""

	// Connection to send packets based on ip version
	if ipVersion == 4 {
		protocol = "ip4:icmp"
		publisherConn, err = net.Dial(protocol, destination.String())
		if err != nil {
			return
		}
		defer publisherConn.Close()
		ipv4Conn := ipv4.NewConn(publisherConn)
		if err = ipv4Conn.SetTTL(ttl); err != nil {
			return
		}
		// request with ipv4 probe packet to send over the network
		request, err = createICMPEchoPacket(ipv4.ICMPTypeEcho)
		if err != nil {
			return
		}
	} else if ipVersion == 6 {
		protocol = "ip6:ipv6-icmp"
		publisherConn, err = net.Dial(protocol, destination.String())
		if err != nil {
			return
		}
		defer publisherConn.Close()
		ipv6Conn := ipv6.NewConn(publisherConn)
		if err = ipv6Conn.SetHopLimit(ttl); err != nil {
			return
		}
		// request with ipv6 probe packet to send over the network
		request, err = createICMPEchoPacket(ipv6.ICMPTypeEchoRequest)
		if err != nil {
			return
		}
	} else {
		return nil, fmt.Errorf("unsupported ip version: %d", ipVersion)
	}

	// Connection to listen for icmp response
	listenerConn, err := icmp.ListenPacket(protocol, source.String())
	if err != nil {
		return
	}
	defer listenerConn.Close()

	// to measure the latency
	start := time.Now()
	_, err = publisherConn.Write(request)
	if err != nil {
		return
	}

	// set icmp echo listening deadline
	err = listenerConn.SetDeadline(time.Now().Add(timeout))
	if err != nil {
		return
	}

	// listen for response
	readBytes := make([]byte, 1500)
	_, sourceAdd, netErr := listenerConn.ReadFrom(readBytes)

	latency := time.Since(start)

	currentHop := &Hop{
		TTL:      ttl,
		Distance: latency,
		Error:    netErr,
	}

	if netErr == nil {
		currentHop.IP = net.ParseIP(sourceAdd.String())
		if currentHop.IP == nil {
			currentHop.Error = errors.New("timeout reached")
		}
	}

	return currentHop, nil
}

// createICMPEchoPacket creates the icmp probe packet
func createICMPEchoPacket(icmpType icmp.Type) ([]byte, error) {
	echoRequest := icmp.Message{
		Type: icmpType,
		Code: 0,
		Body: &icmp.Echo{
			ID:   rand.Int(),
			Seq:  1,
			Data: []byte("TABS"),
		}}
	return echoRequest.Marshal(nil)
}
