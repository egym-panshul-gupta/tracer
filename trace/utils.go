package trace

import (
	"errors"
	"fmt"
	"net"
	"strings"
)

// ParseDestination validates and parse IP destination. If URL, resolves to IP
func ParseDestination(dest string) (net.IP, error) {
	if len(dest) == 0 {
		return nil, errors.New("please provide a valid destination ip or url to trace")
	}
	destIP := net.ParseIP(dest)
	if destIP == nil {
		// resolves ip address from domain, can be IPv4 or IPv6
		destIPs, err := net.LookupIP(dest)
		if err != nil {
			return nil, err
		}
		if len(destIPs) == 0 {
			return nil, errors.New("please provide a valid destination ip or url to trace")
		}
		// for simplicity we will use the first resolved ip address
		destIP = destIPs[0]
	}
	return destIP, nil
}

// DetectSourceAddress detects the first non loopback local address to be used for sending probe packets
func DetectSourceAddress(ipVersion int) (net.IP, error) {
	addresses, err := net.InterfaceAddrs()
	if err != nil {
		return nil, err
	}

	for _, add := range addresses {
		if ipNet, ok := add.(*net.IPNet); ok && !ipNet.IP.IsLoopback() {
			netIP := ipNet.IP
			if ipVersion == 4 && netIP.To4() != nil {
				return netIP, nil
			} else if ipVersion == 6 && netIP.To16() != nil && !strings.HasSuffix(netIP.String(), "::1") {
				// Todo: fix, address returned cannot be bound for listening
				// return netIP, nil
				return net.ParseIP("::0"), nil
			}
		}
	}
	return nil, fmt.Errorf("could not resolve non loopback local IPv%d, please check if connected to network", ipVersion)
}
