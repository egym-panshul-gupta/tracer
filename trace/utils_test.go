package trace

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDetectSourceAddress_v4(t *testing.T) {
	t.Log("testing detect source address with IPv4")
	add, err := DetectSourceAddress(4)
	if assert.NoError(t, err) {
		assert.NotNil(t, add, "source address cannot be null")
		assert.NotEqual(t, "127.0.0.1", add.String(), "source address should not be 127.0.0.1")
		assert.NotEqual(t, "0.0.0.0", add.String(), "source address should not be 0.0.0.0")
		ipv4 := add.To4()
		if ipv4 == nil {
			assert.FailNow(t, "Could not parse source address to ipv4 ", add.String())
		}
	} else {
		assert.FailNow(t, "error while detecting local address", err.Error())
	}
}

func TestDetectSourceAddress_v6(t *testing.T) {
	t.Log("testing detect source address with IPv6")
	add, err := DetectSourceAddress(6)
	if assert.NoError(t, err) {
		assert.NotNil(t, add, "source address cannot be null")
		assert.NotEqual(t, "127.0.0.1", add.String(), "source address should not be 127.0.0.1")
		//assert.NotEqual(t, "::0", add.String(), "source address should not be ::0")
		ipv6 := add.To16()
		if ipv6 == nil {
			assert.FailNow(t, "Could not parse source address to ipv6 ", add.String())
		}
	} else {
		assert.FailNow(t, "error while detecting local address", err.Error())
	}
}

func TestParseDestination(t *testing.T) {
	t.Log("testing parse destination address")
	destIp, err := ParseDestination("www.egym.de")
	if assert.NoError(t, err) {
		assert.NotNil(t, destIp, "destination ip should not be nil")
		assert.NotEqual(t, "www.egym.de", destIp.String(), "destination should be resolved to ip address")
		ipv4 := destIp.To4()
		ipv6 := destIp.To16()
		if ipv4 == nil && ipv6 == nil {
			assert.FailNow(t, "Could not parse destination address to ipv4 or ipv6 ", destIp.String())
		}
	} else {
		assert.FailNow(t, "error while parsing destination ", err.Error())
	}

	destIp, err = ParseDestination("")
	assert.Error(t, err, "should return error for empty destination string")
}
