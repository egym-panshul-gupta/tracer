package trace

import (
	"github.com/stretchr/testify/assert"
	"net"
	"testing"
	"time"
)

func TestTraceSingleHop_v4(t *testing.T) {
	t.Log("testing a single hop IPv4")
	timeout := time.Duration(float64(3) * float64(time.Second.Nanoseconds()))
	ips, lookupErr := net.LookupIP("www.egym.de")
	assert.Nil(t, lookupErr, "Could not lookup test destination ip")
	destination := ips[0] // egym.de
	source, err := DetectSourceAddress(4)
	assert.Nil(t, err, "should be no error while detecting local address")
	assert.NotNil(t, source, "local non loopback address should not be null")
	hop, err := trace(1, destination, source, timeout, 4)
	assert.Nil(t, err, "should be no error while hopping")
	assert.Nil(t, hop.Error, "should have no network error")
	assert.NotNil(t, hop.IP, "hop response should have a source IP")
	assert.NotNil(t, hop.Distance, "hop distance should not be nil")
	t.Logf("%+v\n", hop)
}

// Ignored: did not find a valid IPv6 for testing
//func TestTraceSingleHop_v6(t *testing.T) {
//	t.Log("testing a single hop IPv6")
//	timeout := time.Duration(float64(3) * float64(time.Second.Nanoseconds()))
//	destination := net.ParseIP("2a00:1450:4001:824::2003") // google.de
//	source, err := DetectSourceAddress(6)
//	assert.Nil(t, err, "should be no error while detecting local address")
//	assert.NotNil(t, source, "local non loopback address should not be null")
//	hop, err := trace(64, destination, source, timeout, 6)
//	assert.Nil(t, err, "should be no error while hopping")
//	assert.Nil(t, hop.Error, "should have no network error")
//	assert.NotNil(t, hop.IP, "hop response should have a source IP")
//	assert.NotNil(t, hop.Distance, "hop distance should not be nil")
//	t.Logf("%+v\n", hop)
//}
