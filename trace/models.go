package trace

import (
	"errors"
	"net"
	"time"
)

// Options holds trace command options/settings
// exposes an interface to make the underlying data structure immutable
type Options interface {
	MaxHops() int
	Timeout() int
	Retries() int
	Destination() string
	DestinationIP() net.IP
	SourceIP() net.IP
	IpVersion() int
}

type optionsData struct {
	maxHops     int
	timeout     int
	retries     int
	destination string
	destIp      net.IP
	sourceIp    net.IP
	ipVersion   int
}

func (opts *optionsData) MaxHops() int {
	return opts.maxHops
}

func (opts *optionsData) Timeout() int {
	return opts.timeout
}

func (opts *optionsData) Retries() int {
	return opts.retries
}

func (opts *optionsData) Destination() string {
	return opts.destination
}

func (opts *optionsData) DestinationIP() net.IP {
	return opts.destIp
}

func (opts *optionsData) SourceIP() net.IP {
	return opts.sourceIp
}

func (opts *optionsData) IpVersion() int {
	return opts.ipVersion
}

// OptionsBuilder used to build the immutable object of Options
type OptionsBuilder struct {
	maxHops     int
	timeout     int
	retries     int
	destination string
	destIp      net.IP
	sourceIp    net.IP
	ipVersion   int
}

// NewOptionsBuilder returns an object used to build immutable command options
func NewOptionsBuilder(maxHops int, timeout int, retries int) *OptionsBuilder {
	return &OptionsBuilder{
		maxHops: maxHops,
		timeout: timeout,
		retries: retries,
	}
}

// SetDestination sets destination into builder
func (optsB *OptionsBuilder) SetDestination(dest string) *OptionsBuilder {
	optsB.destination = dest
	return optsB
}

// SetDestinationIP sets destination IP address into builder
func (optsB *OptionsBuilder) SetDestinationIP(ip net.IP) *OptionsBuilder {
	optsB.destIp = ip
	return optsB
}

// SetSourceIP sets destination IP address into builder
func (optsB *OptionsBuilder) SetSourceIP(ip net.IP) *OptionsBuilder {
	optsB.sourceIp = ip
	return optsB
}

// SetIpVersion sets ip version of destination ip address
func (optsB *OptionsBuilder) SetIpVersion(ipVersion int) *OptionsBuilder {
	optsB.ipVersion = ipVersion
	return optsB
}

// Build validates and returns immutable trace options
func (optsB *OptionsBuilder) Build() (Options, error) {
	if optsB.maxHops < 1 {
		return nil, errors.New("max hops cannot be less than 1")
	}
	if optsB.timeout < 1 {
		return nil, errors.New("timeout cannot be less than 1")
	}
	if optsB.retries < 1 {
		return nil, errors.New("retries cannot be less than 1")
	}
	if len(optsB.destination) == 0 {
		return nil, errors.New("destination cannot be empty")
	}
	if optsB.destIp == nil {
		return nil, errors.New("destination ip cannot be nil")
	}
	if optsB.sourceIp == nil {
		return nil, errors.New("source ip cannot be nil")
	}
	if optsB.ipVersion != 4 && optsB.ipVersion != 6 {
		return nil, errors.New("unsupported ip version")
	}
	return &optionsData{
		maxHops:     optsB.maxHops,
		timeout:     optsB.timeout,
		retries:     optsB.retries,
		destination: optsB.destination,
		destIp:      optsB.destIp,
		sourceIp:    optsB.sourceIp,
		ipVersion:   optsB.ipVersion,
	}, nil
}

// Hop represents a node on the route to destination from source.
type Hop struct {
	TrySequence int
	TTL         int
	IP          net.IP
	DNS         []string
	Distance    time.Duration
	Error       error
}
