# Tracer

A minimal implementation of `traceroute` in golang.

**Note: all commands require root access as they make `syscall`**

## Features

* executes a trace to destination ip or url.
* retries all hops for a set number of retries and considers the least latency hop.
* calculates and outputs the two consecutive hops with the maximum latency between them.
* supports IPv4 and IPv6

## Limitations

* currently uses only `icmp` for probing the network

## Known Issues

* might have issues with wireless network, please connect to the wired network and switch off wireless if the trace probe is stuck at a single ip address.
* the local detected source IPv6 cannot be used for listening to icmp response, as a workaround using the local loopback address for IPv6 `::0`. (IPv4 works fine)

## Installation

```bash
go get -u bitbucket.org/egym-panshul-gupta/tracer
```

## Usage

**Note: please run with root access as the tool uses `syscalls`**

### Running the binary

To trace a destination with default values:

```bash
sudo tracer [destination ip or url] 
```

To specify options:

```bash
sudo tracer [flags] [destination ip or url]
```

Possible flags available:

* `-mh`: int : max hops allowed to reach destination (default 64)
* `-rt`: int : number of retries (default 3)
* `-to`: int : timeout for the icmp echo request in seconds (default 3)

To check the current version of binary:

```bash
tracer version
``` 

### Running from the source

Use the go run command in the project root folder with all the above options:

```bash
sudo go run main.go [flags] [destination ip or url]
```

## Building from source

Developed and tested on go version: `go1.12.5`

Please make sure `go` is installed on your system and `$GOPATH` is set. 

Please execute the following commands in the project root folder after cloning the repository.

### Clone Repository

If GO-MOD is enabled then clone the repository in any folder on your system, else clone it inside the `$GOPATH/src`.

Assuming the code is cloned into folder: `/some-path/tracer`

### Testing

To run all the tests, in the root folder

```bash
sudo make test
```

### Build for your local environment

This will detect your local architecture and build a binary for it in the `bin` folder
```bash
make build
```

### Build for MacOS

This will build a binary in the folder `bin/darwin/amd64`

```bash
make build_mac
```

### Build for linux

This will build a binary in the folder `bin/linux/amd64`

```bash
make build_linux
```

### Build for linux and mac platform

This will build the binaries in folders `bin/darwin/amd64` and `bin/linux/amd64` respectively

```bash
make build_all
```


