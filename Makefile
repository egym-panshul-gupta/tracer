test:
	go test -v ./...

build:
	mkdir -p bin
	rm -f ./bin/*.*
	go build -ldflags "-X main.sha1ver=`git rev-parse HEAD` -X main.buildTime=$(date -u +'%Y-%m-%dT%T')" -o bin/tracer ./main.go

build_all: build_mac build_linux

build_mac:
	mkdir -p bin/darwin/amd64
	rm -f ./bin/darwin/amd64/*.*
	env GOOS=darwin GOARCH=amd64 go build -ldflags "-X main.sha1ver=`git rev-parse HEAD` -X main.buildTime=$(date -u +'%Y-%m-%dT%T')" -o bin/darwin/amd64/tracer ./main.go

build_linux:
	mkdir -p bin/linux/amd64
	rm -f ./bin/linux/amd64/*.*
	env GOOS=linux GOARCH=amd64 go build -ldflags "-X main.sha1ver=`git rev-parse HEAD` -X main.buildTime=$(date -u +'%Y-%m-%dT%T')" -o bin/linux/amd64/tracer ./main.go
