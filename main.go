package main

import (
	"bitbucket.org/egym-panshul-gupta/tracer/trace"
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

var (
	sha1ver   string // sha1 revision used to build the program
	buildTime string // when the executable was built
)

func main() {
	optionsBuilder := parseFlags()

	// check and parse destination input
	destination := flag.Arg(0)
	if destination == "version" {
		showVersion()
	}
	destIp, err := trace.ParseDestination(destination)
	handleError(err)
	var ipVersion int

	if destIp.To4() != nil {
		ipVersion = 4
	} else {
		ipVersion = 6
	}

	sourceIP, err := trace.DetectSourceAddress(ipVersion)
	handleError(err)

	optionsBuilder.
		SetSourceIP(sourceIP).
		SetDestinationIP(destIp).
		SetDestination(destination).
		SetIpVersion(ipVersion)

	options, err := optionsBuilder.Build()
	handleError(err)

	fmt.Printf("Tracing from %s to %s (%s), max hops: %d \n", options.SourceIP(), destination, options.DestinationIP(), options.MaxHops())
	hops, err := trace.Exec(options)
	handleError(err)
	trace.CalculateMaxLatencyHop(hops)
}

func handleError(err error) {
	if err != nil {
		log.Printf("ERROR: %v", err.Error())
		showHelp()
		os.Exit(1)
	}
}

func showHelp() {
	fmt.Printf("Usage: tracer version\n")
	fmt.Printf("Usage: tracer [flags] [destination url or ip]\n")
	flag.PrintDefaults()
}

func parseFlags() *trace.OptionsBuilder {
	var maxHops int
	var timeout int
	var retries int
	flag.IntVar(&maxHops, "mh", 64, "max hops allowed to reach destination")
	flag.IntVar(&timeout, "to", 3, "timeout for the icmp echo request in seconds")
	flag.IntVar(&retries, "rt", 3, "number of retries")
	flag.Usage = showHelp
	flag.Parse()
	return trace.NewOptionsBuilder(maxHops, timeout, retries)
}

func showVersion() {
	if sha1ver == "" {
		sha1ver = "local build"
	}

	if buildTime == "" {
		buildTime = fmt.Sprintf(time.Now().UTC().Format("2006-01-02T15:04:05"))
	}

	fmt.Printf("sha1 version: %s\n", sha1ver)
	fmt.Printf("build time: %s\n", buildTime)
	os.Exit(0)
}
